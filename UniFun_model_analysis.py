# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: py3.6
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np
import pickle

# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)

import matplotlib.pyplot as plt
# %matplotlib inline


# Set default font size
plt.rcParams['font.size'] = 24

# Internal ipython tool for setting figure size
from IPython.core.pylabtools import figsize

import seaborn as sb
# Set default font size
sb.set(font_scale = 1.2)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)

# Imputing missing values and scaling values
from sklearn.preprocessing import Imputer, MinMaxScaler

# Machine Learning Models
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn import tree

# LIME for explaining predictions
import lime 
import lime.lime_tabular

# +
# Read in data into dataframes 
train_features = pd.read_csv('data/UniFun_training_features.csv')
train_features.dropna(how="all", inplace=True, axis=1)
test_features = pd.read_csv('data/UniFun_testing_features.csv')
test_features.dropna(how="all", inplace=True, axis=1)
train_labels = pd.read_csv('data/UniFun_training_labels.csv')
test_labels = pd.read_csv('data/UniFun_testing_labels.csv')

# Display sizes of data
print('Training Feature Size: ', train_features.shape)
print('Testing Feature Size:  ', test_features.shape)
print('Training Labels Size:  ', train_labels.shape)
print('Testing Labels Size:   ', test_labels.shape)

train_features.head(12)
# -

for f in train_features.columns:
    print(f)

# +
# Create an imputer object with a median filling strategy
imputer = Imputer(strategy='median')

# Train on the training features
imputer.fit(train_features)

# Transform both training data and testing data
X = imputer.transform(train_features)
X_test = imputer.transform(test_features)

# Sklearn wants the labels as one-dimensional vectors
y = np.array(train_labels).reshape((-1,))
y_test = np.array(test_labels).reshape((-1,))

# load the best model from file
filename = 'data/UniFun_final_model.sav'
model = pickle.load(open(filename, 'rb'))

# +
# Extract the feature importances into a dataframe
feature_results = pd.DataFrame({'feature': list(train_features.columns), 
                                'importance': model.feature_importances_})

# Show the top 10 most important
feature_results = feature_results.sort_values('importance', ascending = False).reset_index(drop=True)

feature_results.head(10)

# +
figsize(12, 10)

# Plot the 10 most important features in a horizontal bar chart
feature_results.loc[:9, :].plot(x = 'feature', y = 'importance', 
                                 edgecolor = 'k',
                                 kind='barh', color = 'blue');
plt.xlabel('Relative Importance', size = 15); plt.ylabel('')
plt.title('Feature Importances from Random Forest', size = 20);

# +
# Extract the names of the most important features
most_important_features = feature_results['feature'][:10]

# Find the index that corresponds to each feature name
indices = [list(train_features.columns).index(x) for x in most_important_features]

# Keep only the most important features
X_reduced = X[:, indices]
X_test_reduced = X_test[:, indices]

print('Most important training features shape: ', X_reduced.shape)
print('Most important testing  features shape: ', X_test_reduced.shape)

# +
lr = LogisticRegression()

# Function to calculate mean absolute error
def mae(y_true, y_pred):
    return np.mean(abs(y_true - y_pred))

# Fit on full set of features
lr.fit(X, y)
lr_full_pred = lr.predict(X_test)

# Fit on reduced set of features
lr.fit(X_reduced, y)
lr_reduced_pred = lr.predict(X_test_reduced)

# Display results
print('Linear Regression Full Results: MAE =    %0.4f.' % mae(y_test, lr_full_pred))
print('Linear Regression Reduced Results: MAE = %0.4f.' % mae(y_test, lr_reduced_pred))

# +
# Create the model with the best set of hyperparamters
model_reduced = RandomForestClassifier(
    n_estimators = 60,
    max_leaf_nodes = None,
    random_state = 60,
    verbose = 0,
    min_samples_leaf = 1,
    warm_start = False,
    max_features = "auto",
    criterion = "gini",
    class_weight = None,
    bootstrap = False,
    min_samples_split = 5,
    min_weight_fraction_leaf = 0.0,
    n_jobs = 1,
    min_impurity_split = None,
    max_depth = 30,
    oob_score = False,
    min_impurity_decrease = 0.0)

# Fit and test on the reduced set of features
model_reduced.fit(X_reduced, y)
model_reduced_pred = model_reduced.predict(X_test_reduced)

print('Random Forest Reduced Results: MAE = %0.4f' % mae(y_test, model_reduced_pred))

# +
# LIME (Locally Interpretable Model-agnostic Explanations) analysis

# Find the residuals
residuals = abs(model_reduced_pred - y_test)
    
# Exact the worst and best prediction
wrong = X_test_reduced[np.argmax(residuals), :]
right = X_test_reduced[np.argmin(residuals), :]
# -

# Create a lime explainer object
explainer = lime.lime_tabular.LimeTabularExplainer(training_data = X_reduced, 
                                                   mode = 'regression',
                                                   training_labels = y,
                                                   feature_names = list(most_important_features))

# +
# Display the predicted and true value for the wrong instance
print('Prediction: %0.4f' % model_reduced.predict(wrong.reshape(1, -1)))
print('Actual Value: %0.4f' % y_test[np.argmax(residuals)])

# Explanation for wrong prediction
wrong_exp = explainer.explain_instance(data_row = wrong, 
                                       predict_fn = model_reduced.predict)

# Plot the prediction explaination
wrong_exp.as_pyplot_figure();
plt.title('Explanation of False Positive Prediction', size = 28);
plt.xlabel('Effect on Prediction', size = 22);
# -

print(most_important_features)
print(wrong)

wrong_exp.save_to_file("figures/UniFun_wrong_randomforest_reduced.html")

# +
# Display the predicted and true value for the right instance
print('Prediction: %0.4f' % model_reduced.predict(right.reshape(1, -1)))
print('Actual Value: %0.4f' % y_test[np.argmin(residuals)])

# Explanation for right prediction
right_exp = explainer.explain_instance(right, model_reduced.predict, num_features=10)
right_exp.as_pyplot_figure();
plt.title('Explanation of True Negative Prediction', size = 28);
plt.xlabel('Effect on Prediction', size = 22);
# -

right_exp.save_to_file("figures/UniFun_right_randomforest_reduced.html")

# +
# Extract a single tree
single_tree = model_reduced.estimators_[42]

tree.export_graphviz(single_tree, out_file = 'data/UniFun_randomforest_singletree.dot',
                     rounded = True, 
                     feature_names = most_important_features,
                     filled = True, max_depth = 3,
                    rotate=True)
single_tree
# -

# jupyter nbconvert --to markdown *.ipynb
# !dot -Tpng data/randomforest_singletree.dot -o figures/UniFun_randomforest_singletree.png

# ![image](randomforest_singletree.png)
