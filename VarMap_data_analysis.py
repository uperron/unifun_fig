# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: py3.6
#     language: python
#     name: python3
# ---

# # A structurally-aware variant effect model
#
#
#
#
#
# ## Introduction
#
# Genetic variant effect prediction algorithms are used extensively in clinical genomics and research to determine the likely consequences of amino acid substitutions on protein function and their pathogenic significance. Popular variant effect prediction methods include [SIFT](https://dx.doi.org/10.1093/nar/gks539), [PolyPhen (v2)](https://dx.doi.org/10.1038/nmeth0410-248), [CADD](https://dx.doi.org/10.1038/ng.2892), [fathmm](https://dx.doi.org/10.1002%2Fhumu.22225) and [REVEL](https://reference.medscape.com/viewpublication/2412). These use information about local sequence phylogenetic conservation, amino acid physicochemical properties, functional domains and amino acid structural attributes in the case of PolyPhen. CADD and REVEL instead integrate and weight predictions from collections of tools.
# Variant predictors are widely used in practice, however their performance is strongly influenced by the choice and composition of variant datasets used for training and validation. REVEL for example achieves a ROC AUC of .96 when tested against a set of variants from Clinvar and of .63 when benchmarked against an assay-validated variant [dataset](https://doi.org/10.1186/s40246-017-0104-8).
# Thus, the devopment of more robust variant effect models is a viable area of research with a large translational potential. In this context residue-specific structural features could play an important role alogside sequence conservation and domain features; particularly when cosidering the relationship between protein structure and function and the increase in quality and quantity of solved strucutures. The strucutural features used here differ from dose in tools such as PolyPhen since they are computed for each residue in its unique local context as opposed to being general amino acid structural features computed across many different structures.
#
#
# ## Workflow
#
# In order to compute structural features it is necessary to map a variant's coordinates in the genome to the corresponding residue in our model of the protein coded by that genome region. Tools such as [VarMap](https://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/VarSite/GetPage.pl?uniprot_acc=P01308&template=VarMap.html) have automated this procedure. Here, a pre-compiled subset of ClinVar annotated using VarMap is used as well as structural features computed for each residue using [DSSP](http://swift.cmbi.ru.nl/gv/dssp/), [get_cleft](https://github.com/NRGlab/Get_Cleft) and [biopython](https://github.com/biopython/biopython).
#
# Code for all the steps above is available on [bitbucket](https://bitbucket.org/uperron/unifun).

# +
# this borrows from https://github.com/WillKoehrsen/machine-learning-project-walkthrough

import pandas as pd
import numpy as np
import pickle

# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)

import matplotlib.pyplot as plt
# %matplotlib inline


# Set default font size
plt.rcParams['font.size'] = 24

# Internal ipython tool for setting figure size
from IPython.core.pylabtools import figsize

import seaborn as sb
# Set default font size
sb.set(font_scale = 1.1)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)

# Splitting data into training and testing
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
# -

data = pd.read_csv("data/VarMap_BOTH.ALLfeatures.merged", sep="\t", header=0, 
                   index_col=None)
data.drop_duplicates(inplace=True)
data.head()

data.info()

# +
# drop ID column
data.drop(["ID", "DSSP_AA"], 1, inplace=True)


# general NA replacement
data = data.replace({'NA': np.nan})
# object-type columns require special care,
# these should be numerical
for col in ['POLYPHEN_SCORE','SIFTS_SCORE','CADD_PHRED','CADD_RAW', 
            "NAT_VARIANTS", "GNOMAD_ALLELE_FREQ", "NTO_PROTEIN"]:
    data[col] = pd.to_numeric(data[col], errors='coerce')

# handling missing data in specific cols
# missing cleft_size values are filled with 0
data["cleft_size"].fillna(0, inplace=True)
# encoding new in_cleft feature
data["in_cleft"] = data["cleft_size"].apply(lambda x: 1 if x != 0 else 0)
# missing cleft_size_rank values are filled with the highest rank observed
M = data["cleft_size_rank"].max()
data["cleft_size_rank"].fillna(M, inplace=True)

# encoding new in_domain features
data["in_PFAM"] = data["PFAM_DOMAIN"].apply(lambda x: 1 if x != '-' else 0)
data["in_CATH"] = data["CATH_DOMAIN"].apply(lambda x: 1 if x != '-' else 0)

# sum number of variant association in 
# diseases , mutagenesis_expts and ClinVar data
data[["disease_assoc_count", "mutagenesis_expts_assoc_count", 
      "ClinVar_assoc_count"]] = data.apply(lambda row: [int(row['NVARIANTS'].split(";")[i]) for i in [0,1,4]], 
                                                        axis=1, result_type="expand")
# -

data.info()

data.describe()

data.isnull().sum()


# Function to calculate missing values by column
# from https://stackoverflow.com/a/39734251
def missing_values_table(df):
        # Total missing values
        mis_val = df.isnull().sum()
        
        # Percentage of missing values
        mis_val_percent = 100 * df.isnull().sum() / len(df)
        
        # Make a table with the results
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
        
        # Rename the columns
        mis_val_table_ren_columns = mis_val_table.rename(
        columns = {0 : 'Missing Values', 1 : '% of Total Values'})
        
        # Sort the table by percentage of missing descending
        mis_val_table_ren_columns = mis_val_table_ren_columns[
            mis_val_table_ren_columns.iloc[:,1] != 0].sort_values(
        '% of Total Values', ascending=False).round(1)
        
        # Print some summary information
        print ("Your selected dataframe has " + str(df.shape[1]) + " columns.\n"      
            "There are " + str(mis_val_table_ren_columns.shape[0]) +
              " columns that have missing values.")
        
        # Return the dataframe with missing information
        return mis_val_table_ren_columns


#rule of thumb: reject columns with >50% missing values
missing_values_table(data)

# drop cols with more than 60% null values
data.drop(["NAT_VARIANTS", "ClinVar_assoc_count"], 1, inplace=True)

data["SS_DSSP"].value_counts()

# +
# Histogram of the score variables

# PolyPhen-2 and SIFT scores use the same range, 0.0 to 1.0, but with opposite meanings. 
# A variant with a PolyPhen score of 0.0 is predicted to be benign. 
# A variant with a SIFT score of 1.0 is predicted to be benign. 
# Of tbe 3 VEPs PolyPhen-2 is the only one to use structural features (residue Bfactor, exposure)
# CADD uses conservation metrics, functional genomic data,  transcript information, 

all_score_cols =  ["POLYPHEN_SCORE", "SIFTS_SCORE", "CADD_PHRED", "CADD_RAW"]

# CADD_RAW values have no absolute unit of meaning and are incomparable 
# across distinct annotation combinations, training sets, or model parameters.
# The scaled variant rank, CADD_PHRED, is used.
score_cols = ["POLYPHEN_SCORE", "SIFTS_SCORE", "CADD_PHRED"]
for col in score_cols:
    plt.hist(data[col].dropna(), bins = 100, edgecolor = 'k');
    plt.xlabel(col); plt.ylabel('Number of variants'); 
    plt.title("Raw counts by %s" % (col), size = 22);
    plt.show();

# +
#The PolyPhen-2 score ranges from 0.0 (tolerated) to 1.0 (deleterious). 
# The score can be interpreted as follows:
#0.0 to 0.15 -- Variants with scores in this range are predicted to be benign.
#0.85 to 1.0 -- Variants with scores in this range are more confidently predicted to be damaging.
print(data["POLYPHEN_SCORE"].describe())
# discretize the poliphen score according to the quantiles above
discrete_polyphen_score = data["POLYPHEN_SCORE"].apply(lambda x: 0 if x < .15 else (1 if x > .85 else np.nan)) 
print(discrete_polyphen_score.isnull().sum())
print(discrete_polyphen_score.notnull().sum())
print(discrete_polyphen_score.dropna().value_counts())

plt.hist(discrete_polyphen_score.dropna(), bins = 100, edgecolor = 'k');
plt.xlabel(col); plt.ylabel('Number of variants'); 
plt.title("Raw counts by discrete_polyphen_score", size = 22);
plt.show();
data["POLIPHEN_DISCRETE_SCORE"] = discrete_polyphen_score
all_score_cols =  all_score_cols + ["POLIPHEN_DISCRETE_SCORE"]

# +
# Score distibution by secondary structure
figsize(12, 10)

ss_types = data["SS_DSSP"].value_counts()
# only sencondary structure classes with more than 1000 variants
ss_types = list(ss_types[ss_types.values > 100].index)

for score_col in score_cols:
    for ss_type in ss_types:
        subset = data.query('SS_DSSP == @ss_type')[["SS_DSSP", score_col]]
        sb.kdeplot(subset[score_col].dropna(),
                   label = ss_type, shade = False, alpha = 0.8);

    plt.xlabel(score_col, size = 20); plt.ylabel('Density', size = 20); 
    plt.title("Density Plot of %s by II structure type" % (score_col), size = 22);
    plt.show();

# +
# polyphen discrete score distibution by secondary structure
from itertools import product

figsize(12, 10)

ss_types = data["SS_DSSP"].value_counts()
# only sencondary structure classes with more than 1000 variants
ss_types = list(ss_types[ss_types.values > 100].index)
subset = data.query("SS_DSSP in @ss_types")[['POLIPHEN_DISCRETE_SCORE', 'SS_DSSP']].dropna()

# normalize by number of variants in score class
arr = []
for t,s in product(ss_types, [1, 0]):
    n = len(subset.query("POLIPHEN_DISCRETE_SCORE ==@s & SS_DSSP == @t"))
    N = len(subset[subset['POLIPHEN_DISCRETE_SCORE'] == s])
    arr.append([t,s, n/N])
plot_df = pd.DataFrame(arr, columns=["SS_DSSP", "POLIPHEN_DISCRETE_SCORE", "norm_counts"])

sb.barplot(x = 'SS_DSSP', y="norm_counts", hue="POLIPHEN_DISCRETE_SCORE", data=plot_df);
plt.xlabel('POLIPHEN_DISCRETE_SCORE', size = 20); plt.ylabel('Counts', size = 20); 
plt.title("Normalized counts by POLIPHEN_DISCRETE_SCORE and II structure type", size = 22);

# +
# Score distibution by change type
figsize(12, 10)
chg_types = data["CHANGE_TYPE"].value_counts()
# only sencondary structure classes with more than 1000 variants
chg_types = list(chg_types[chg_types.values > 200].index)

# remove rare CHANGE_TYPE levels
data["CHANGE_TYPE"] = data["CHANGE_TYPE"].apply(lambda x: x if x in chg_types else np.nan)

# +
# Score distibution by change type
figsize(12, 10)
chg_types = data["CHANGE_TYPE"].value_counts()
# only sencondary structure classes with more than 1000 variants
chg_types = list(chg_types[chg_types.values > 200].index)

for score_col in score_cols:
    for chg_type in chg_types:
        subset = data.query('CHANGE_TYPE == @chg_type')[score_col].dropna()
        sb.kdeplot(subset, label = chg_type, shade = False, alpha = 0.8);

    plt.xlabel(score_col, size = 20); plt.ylabel('Density', size = 20); 
    plt.title("Density Plot of %s by VEP CHANGE_TYPE" % (score_col), size = 22);
    plt.show();

# +
# polyphen discrete score distibution by secondary structure
from itertools import product

fig, ax = plt.subplots(figsize=(12, 10))

subset = data.query("CHANGE_TYPE in @chg_types")[['POLIPHEN_DISCRETE_SCORE', 'CHANGE_TYPE']].dropna()

# normalize by number of variants in score class
arr = []
for t,s in product(chg_types, [1, 0]):
    n = len(subset.query("POLIPHEN_DISCRETE_SCORE ==@s & CHANGE_TYPE == @t"))
    N = len(subset[subset['POLIPHEN_DISCRETE_SCORE'] == s])
    #N = len(subset[subset['CHANGE_TYPE'] == t])
    arr.append([t,s, n/N])
plot_df = pd.DataFrame(arr, columns=["CHANGE_TYPE", "POLIPHEN_DISCRETE_SCORE", "norm_counts"])

sb.barplot(x = 'CHANGE_TYPE', y="norm_counts", hue="POLIPHEN_DISCRETE_SCORE", data=plot_df, ax=ax);

ax.set(xticklabels=chg_types)
plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
plt.xlabel('CHANGE_TYPE', size = 20); plt.ylabel('Counts', size = 20); 
plt.title("Normalized counts by POLIPHEN_DISCRETE_SCORE and CHANGE_TYPE", size = 22);
# -

plot_df = data.query("CHANGE_TYPE in @chg_types")[['POLIPHEN_DISCRETE_SCORE', 'CHANGE_TYPE']].dropna()
g = sb.catplot("POLIPHEN_DISCRETE_SCORE", col="CHANGE_TYPE", col_wrap=3,
 data=plot_df, kind="count", sharey=False, order=[0,1]);
(g.set_axis_labels("", "Counts")
.set_titles("{col_name}"))

for score_col in score_cols + ["POLIPHEN_DISCRETE_SCORE"]:
    subset = data.drop(all_score_cols, 1)
    subset[score_col] = data[score_col]
    # Find all correlations and sort 
    correlations_data = subset.corr()[score_col].sort_values()
    print("Correlations with %s" % (score_col))
    print(correlations_data, '\n')


# +
# Select the numeric columns
numeric_subset = data.drop(all_score_cols, 1).select_dtypes('number')

# Create columns with square root and log of numeric columns
for col in numeric_subset.columns:
    numeric_subset['sqrt_' + col] = np.sqrt(numeric_subset[col])
    numeric_subset['log_' + col] = np.log(numeric_subset[col])

# Select the categorical columns
categorical_subset = data[["SS_DSSP", "CHANGE_TYPE"]]

# One hot encode
categorical_subset = pd.get_dummies(categorical_subset)

# Join the two dataframes using concat
# Make sure to use axis = 1 to perform a column bind
features = pd.concat([numeric_subset, categorical_subset], axis = 1)
features.columns = [col.replace(" ", "_") for col in features.columns]

subset = features
# Find correlations with the scores
for score_col in score_cols:
    subset["score"] = data[score_col]
    # Find all correlations and sort 
    correlations_data = subset.corr()["score"].dropna().sort_values()
    print("Correlations with %s" % (score_col))
    print(correlations_data.head(10))
    print(correlations_data.tail(10), '\n')

# +
# Extract the columns to  plot
plot_data = features[['CHANGE_TYPE_Stop_gained', 
                      'CHANGE_TYPE_Synonymous_variant', 
                      'EXP_DSSP_RASA',
                     "log_GNOMAD_ALLELE_FREQ", 
                      "sqrt_disease_assoc_count"]]
                    
plot_data["POLYPHEN_SCORE"] = data["POLYPHEN_SCORE"]
plot_data["CADD_PHRED"] = data["CADD_PHRED"]

# Replace the inf with nan
plot_data = plot_data.replace({np.inf: np.nan, -np.inf: np.nan}).dropna()

# Function to calculate correlation coefficient between two columns
def corr_func(x, y, **kwargs):
    r = np.corrcoef(x, y)[0][1]
    ax = plt.gca()
    ax.annotate("r = {:.2f}".format(r),
                xy=(.2, .8), xycoords=ax.transAxes,
                size = 20)

# Create the pairgrid object
grid = sb.PairGrid(data = plot_data, size = 3)

# Upper is a scatter plot
grid.map_upper(plt.scatter, alpha = 0.6, color="g")

# Diagonal is a histogram
grid.map_diag(plt.hist, edgecolor = 'black')

# Bottom is correlation and density plot
grid.map_lower(corr_func);
grid.map_lower(sb.kdeplot, cmap = plt.cm.Reds)

# Title for entire plot
plt.suptitle('Pairs Plot of features', size = 20, y = 1.02);

# +
# From  this point onward use the discretized POLIPHEN_DISCRETE_SCORE
# as a high(er) confidence classification to train and test the model.
# Select the numeric columns
Y = data["POLIPHEN_DISCRETE_SCORE"]
numeric_subset = data.drop(all_score_cols, 1).select_dtypes('number')

# Create columns with square root and log of numeric columns
for col in numeric_subset.columns:
    numeric_subset['sqrt_' + col] = np.sqrt(numeric_subset[col])
    numeric_subset['log_' + col] = np.log(numeric_subset[col])

# Select the categorical columns
categorical_subset = data[["SS_DSSP", "CHANGE_TYPE"]]

# One hot encode
categorical_subset = pd.get_dummies(categorical_subset)

# Join the two dataframes using concat
# Make sure to use axis = 1 to perform a column bind
features = pd.concat([numeric_subset, categorical_subset], axis = 1)
features.columns = [col.replace(" ", "_") for col in features.columns]
# add POLIPHEN_DISCRETE_SCORE back in and use it as score
features["score"] = Y
features.shape


# -

def remove_collinear_features(x, threshold):
    #        Remove collinear features in a dataframe with a correlation coefficient
    #        greater than the threshold. Removing collinear features can help a model
    #        to generalize and improves the interpretability of the model.

    # Dont want to remove correlations between Energy Star Score
    y = x['score']
    x = x.drop(columns = ['score'])
    
    # Calculate the correlation matrix
    corr_matrix = x.corr()
    iters = range(len(corr_matrix.columns) - 1)
    drop_cols = []

    # Iterate through the correlation matrix and compare correlations
    for i in iters:
        for j in range(i):
            item = corr_matrix.iloc[j:(j+1), (i+1):(i+2)]
            col = item.columns
            row = item.index
            val = abs(item.values)
            
            # If correlation exceeds the threshold
            if val >= threshold:
                # Print the correlated features and the correlation value
                # print(col.values[0], "|", row.values[0], "|", round(val[0][0], 2))
                drop_cols.append(col.values[0])

    # Drop one of each pair of correlated columns
    drops = set(drop_cols)
    x = x.drop(columns = drops)
    
    # Add the score back in to the data
    x['score'] = y
               
    return x


# Remove the collinear features above a specified correlation coefficient
features = remove_collinear_features(features, 0.6);

# Remove any columns with all na values
features  = features.dropna(axis=1, how = 'all')
features.shape

# +
score = features[features['score'].notnull()]
# Separate out the features and targets
features = score.drop(columns='score')
targets = pd.DataFrame(score['score'])

# Replace the inf and -inf with nan (required for later imputation)
features = features.replace({np.inf: np.nan, -np.inf: np.nan})

# Split into 70% training and 30% testing set
X, X_test, y, y_test = train_test_split(features, targets, test_size = 0.3, random_state = 42)

print(X.shape)
print(X_test.shape)
print(y.shape)
print(y_test.shape)
# -

print(features.columns)

# +
from sklearn.metrics import confusion_matrix

# https://scikit-learn.org/stable/auto_examples/model_selection/\
# plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
# Plot the confusion matrix to evaluate the quality of the output of a classifier.
# The diagonal elements represent the number of points for which the 
# predicted label is equal to the true label, while off-diagonal elements 
# are those that are mislabeled by the classifier. The higher the diagonal 
# values of the confusion matrix the better, indicating many correct predictions.
def plot_confusion_matrix(y_true, y_pred, normalize=False, cmap=plt.cm.Blues):

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Compute accuracy
    accuracy = np.sum(np.diagonal(cm)) / np.sum(cm)
    # Only use the labels that appear in the data
    classes = sorted(unique_labels(y_true, y_pred))
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        title = "Normalized confusion matrix, accuracy = %f " % (accuracy)
    else:
        title = "Confusion matrix, accuracy = %f " % (accuracy)


    fig, ax = plt.subplots(figsize=(8, 7))
    im = sb.heatmap(cm,  annot=True, fmt="d", 
                    linewidth=.5,
                    linecolor="k",
                    cmap=cmap)
    ax.set(xticklabels=["Benign", "Pathogenic"], yticklabels=["Benign", "Pathogenic"])
    # We want to show all ticks...
    ax.set(ylabel='True label',
          xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    plt.suptitle(title, size = 20, y = 1.02);
    fig.tight_layout()
    return ax

# Function to calculate mean absolute error
def mae(y_true, y_pred):
    return np.mean(abs(y_true - y_pred))



# -

# Baseline guess is based on distribution of POLYPHEN_SCORE in training set
baseline_df = y['score'].value_counts().apply(lambda x: x / len(y))
classes = baseline_df.index.values
probs = baseline_df.values
baseline_guess = np.random.choice(classes, size=len(y_test), p=probs)
plot_confusion_matrix(y_test, baseline_guess);
print("Baseline mean abbsolute error: %.4f" % mae(y_test.values, baseline_guess));


X.to_csv('data/VarMap_training_features.csv', index = False)
X_test.to_csv('data/VarMap_testing_features.csv', index = False)
y.to_csv('data/VarMap_training_labels.csv', index = False)
y_test.to_csv('data/VarMap_testing_labels.csv', index = False)


