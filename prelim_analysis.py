# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np

# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)

import matplotlib.pyplot as plt
# %matplotlib inline


# Set default font size
plt.rcParams['font.size'] = 24

# Internal ipython tool for setting figure size
from IPython.core.pylabtools import figsize

import seaborn as sb
# Set default font size
sb.set(font_scale = 1.75)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("darkgrid", rc=custom_style)

# -

data = pd.read_csv("data/VarMAp_ClinVar.tsv", sep="\t", index_col=None, 
                   header=0, error_bad_lines=False)
data.head()

data.info()


data["HAVE_PDB"].value_counts()

data["PDB_UNIPROT_MATCH"].value_counts()

len(data[data["RES_NUM"] != "-"])

X = data[data["PDB_RESOLUTION"] != '-']["PDB_RESOLUTION"].dropna().astype(float)
sb.distplot(X[X < 3])

X = data[data["PDB_RESOLUTION"] != '-']["PDB_RESOLUTION"].dropna().astype(float)
len(X)

X = data[data["CADD_PHRED"]!= '-']["CADD_PHRED"].dropna().astype(float)
sb.distplot(X)

out_data = data[(data["HAVE_PDB"] == True) & (data["PDB_RESOLUTION"] != "-")]
out_data["PDB_RESOLUTION"] = out_data["PDB_RESOLUTION"].astype(float)
out_data = out_data[out_data["PDB_RESOLUTION"] <3]
out_data["ID"] = [i for i in range(len(out_data))]
out_data.to_csv("VarMAp_ClinVar.filter.tsv", sep="\t", index=None)

out_data["ID"].head()

out_data.CODON_CHANGE.value_counts()
