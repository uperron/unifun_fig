#!/usr/bin/env Rscript

library(parallel)
library(rtracklayer)
library(ensembldb)
library(EnsDb.Hsapiens.v86)
library(Biostrings)
library(biomaRt)


args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  stop("At least two argument must be supplied (input file, UCSC chain file).n", call.=FALSE)
} else if (length(args)==2) {
  # default output file
  args[3] = "out.txt"
}


# UCSC chain file to convert b/w genome versions
# using LiftOver downloaded from 
# https://hgdownload.cse.ucsc.edu/goldenpath/hg19/liftOver/hg19ToHg38.over.chain.gz
path = args[2]
ch = import.chain(path)

# setup params for biomart query
ensembl = useMart("ensembl",dataset="hsapiens_gene_ensembl")

map_coords <- function(ID, chr_19, pos_19) {
    gnm_pos <- GRanges(chr_19, IRanges(pos_19, width = 1))

    edb <- EnsDb.Hsapiens.v86
    seqlevelsStyle(edb) <- "UCSC"

    # liftOver GRCh37/hg19 -> GRCh38/hg38
    gnm_pos38 <- liftOver(gnm_pos, ch)[[1]]
    chr_38 <- as.character(seqnames(gnm_pos38))
    pos_38 <- start(gnm_pos38)

    ## iRestrict all further queries to the correct chromosome to speed up
    edb <- filter(EnsDb.Hsapiens.v86, filter = ~ seq_name == chr_38)
    seqlevelsStyle(edb) <- "UCSC"

    # map a genome position to a protein position
    prt_pos <- genomeToProtein(gnm_pos38, edb)
    # check for output
    if(length(prt_pos[[1]]) == 0) {stop()}


    prt_pos_df <- as.data.frame(prt_pos[[1]])
    prt_pos_df$cds_ok <- mcols(prt_pos[[1]])$cds_ok
    prt_pos_df$ENST_id <- mcols(prt_pos[[1]])$tx_id
    prt_pos_df <- subset(prt_pos_df, cds_ok == TRUE)


    # get the protein sequence and extract the relevant AA
    prt_seq <- proteins(edb, return.type = "AAStringSet",
                        filter = ~ protein_id == prt_pos_df$names)
    prt_pos_df$AA <-  rep(c("NA"), each=length(prt_pos_df$ENST_id))  
    try(prt_pos_df$AA <- as.character(subseq(prt_seq, start = prt_pos_df$start, end = prt_pos_df$end)))
    colnames(prt_pos_df) <- c("prt_start", "prt_end", "prt_width",
                              "ENSP_id", "cds_ok", "ENST_id","AA")

    # map a protein residue to the corresponding codon position range
    # in the coding sequence
    prt_test <- subset(prt_pos[[1]], mcols(prt_pos[[1]])$cds_ok == TRUE)
    gen_test <- proteinToGenome(prt_test, edb)
    arr <- list()
    i <- 1
    for (gr in gen_test){
        trs_pos <- genomeToTranscript(gr, edb)[[1]]
        cds_pos <- transcriptToCds(trs_pos, edb)
        cds_pos <- subset(cds_pos, width(cds_pos) == 3)
        df <- as.data.frame(cds_pos)
        arr[i] = list(df)
        i <- i +1 
    }
    cds_pos_df <- do.call(rbind, arr)
    cds_pos_df <- unique(cds_pos_df[c("start", "end", "width", "names")])
    colnames(cds_pos_df) <- c("trs_start", "trs_end", "trs_width", "ENST_id")

    # Merge the two data frames
    prt_pos_df <- merge(prt_pos_df, cds_pos_df, "ENST_id")

    # get the coding sequence (cds) through biomart, extract the relevant codon sequence
    trs_seq_df <- data.frame(ENST_id=character(), ENST_seq=character())
    try(trs_seq_df <- getBM(attributes=c('ensembl_transcript_id', 'coding'), 
      filters = 'ensembl_transcript_id', 
      values = prt_pos_df$ENST_id, 
      mart = ensembl))    
    colnames(trs_seq_df) <- c("ENST_id", "ENST_seq")

    get_codon <- function(start, end, id){
        l <- strsplit(subset(trs_seq_df, ENST_id == id)$ENST_seq, "")[[1]]
        s <- paste(l[start:end], collapse='')
        s
    }
    
    prt_pos_df$codon <- rep(c("NA"), each=length(prt_pos_df$ENST_id))
    try(prt_pos_df$codon <- mapply(get_codon, prt_pos_df["trs_end"][,1], 
                               prt_pos_df["trs_start"][,1], prt_pos_df["ENST_id"][,1]))
    
    # map the variant genome position to its relative position
    # within the codon
    trs_pos <- genomeToTranscript(gnm_pos38, edb)
    cds_pos <- transcriptToCds(trs_pos[[1]], edb)
    cds_pos_df <- as.data.frame(cds_pos)
    cds_pos_df <- unique(cds_pos_df[c("start", "end", "width", "names")])
    colnames(cds_pos_df) <- c("var_trs_start", "var_trs_end", "var_trs_width", "ENST_id")
    prt_pos_df <- merge(prt_pos_df, cds_pos_df, "ENST_id")
    
    # add the variant ID
    prt_pos_df$ID <- rep(c(toString(ID)),each=length(prt_pos_df$ENST_id)) 
    
    # translate the codon into its corresponding AA
    codonToAA <- function(codon) {GENETIC_CODE[codon]} 
    prt_pos_df$AA_check <- unlist(lapply(prt_pos_df["codon"], codonToAA))
    prt_pos_df$var_pos_codon <- prt_pos_df$var_trs_start - prt_pos_df$trs_start
    out <- prt_pos_df[c("ID", "ENSP_id", "prt_start",
                        "AA", "trs_start", "trs_end", "ENST_id", 
                        "codon", "AA_check", "var_pos_codon")]
    
    write.table(out, args[3], sep = "\t", col.names = FALSE, 
                row.names = FALSE, append = TRUE)  
}

data <- read.csv(file=args[1], header=FALSE, sep="\t")
colnames(data) <- c("CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "PRED")

# leave 2 cores free
mcmapply(map_coords, data["ID"][,1], data["CHROM"][,1], data["POS"][,1],
	 mc.cores=detectCores()-2)

