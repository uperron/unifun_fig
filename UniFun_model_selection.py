# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: py3.6
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np
import pickle

# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)

import matplotlib.pyplot as plt
# %matplotlib inline


# Set default font size
plt.rcParams['font.size'] = 24

# Internal ipython tool for setting figure size
from IPython.core.pylabtools import figsize

import seaborn as sb
# Set default font size
sb.set(font_scale = 1.1)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)

# Imputing missing values and scaling values
from sklearn.preprocessing import Imputer, MinMaxScaler

# Machine Learning Models
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import roc_curve, confusion_matrix, auc, matthews_corrcoef
# Hyperparameter tuning
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV

# +
# Read in data into dataframes 
train_features = pd.read_csv('data/UniFun_training_features.csv')
test_features = pd.read_csv('data/UniFun_testing_features.csv')
train_labels = pd.read_csv('data/UniFun_training_labels.csv')
test_labels = pd.read_csv('data/UniFun_testing_labels.csv')

# Display sizes of data
print('Training Feature Size: ', train_features.shape)
print('Testing Feature Size:  ', test_features.shape)
print('Training Labels Size:  ', train_labels.shape)
print('Testing Labels Size:   ', test_labels.shape)
# -

train_features.head(12)

len(train_features.columns)

print('Missing values in training features: ', np.sum(np.isnan(train_features)))
print('Missing values in testing features:  ', np.sum(np.isnan(test_features)))

# +
# Create an imputer object with a median filling strategy
imputer = Imputer(strategy='median')

# Train on the training features
imputer.fit(train_features)

# Transform both training data and testing data
X = imputer.transform(train_features)
X_test = imputer.transform(test_features)
# -

print(train_features.shape)
print(X.shape)

print('Missing values in training features: ', np.sum(np.isnan(X)))
print('Missing values in testing features:  ', np.sum(np.isnan(X_test)))

# +
# Create the scaler object with a range of 0-1
scaler = MinMaxScaler(feature_range=(0, 1))

# Fit on the training data
scaler.fit(X)

# Transform both the training and testing data
X = scaler.transform(X)
X_test = scaler.transform(X_test)
# -

# Convert y to one-dimensional array (vector)
y = np.array(train_labels).reshape((-1, ))
y_test = np.array(test_labels).reshape((-1, ))


# +
# Function to calculate mean absolute error
def mae(y_true, y_pred):
    return np.mean(abs(y_true - y_pred))

def evalBinaryClassifier(model, x, y, labels=['Pathogenic','Benign']):
    '''
    https://towardsdatascience.com/how-to-interpret-a-binary\
    -logistic-regressor-with-scikit-learn-6d56c5783b49
    Visualize the performance of  a Logistic Regression Binary Classifier.
    
    Displays a labelled Confusion Matrix, distributions of the predicted
    probabilities for both classes, the ROC curve, and F1 score of a fitted
    Binary Logistic Classifier. Author: gregcondit.com/articles/logr-charts
    
    Parameters
    ----------
    model : fitted scikit-learn model with predict_proba & predict methods
        and classes_ attribute. Typically LogisticRegression or 
        LogisticRegressionCV
    
    x : {array-like, sparse matrix}, shape (n_samples, n_features)
        Testing features vector, where n_samples is the number of samples
        in the data to be tested, and n_features is the number of features
    
    y : array-like, shape (n_samples,)
        Target vector relative to x.
    
    labels: list, optional
        list of text labels for the two classes, with the positive label first
        
    Displays
    ----------
    3 Subplots
    
    Returns
    ----------
    F1: float
    '''
    model_name = str(model).split("(")[0]
    #model predicts probabilities of positive class
    p = model.predict_proba(x)
    model_pred = model.predict(x)
    model_mae = mae(y, model_pred)
    model_mcc = matthews_corrcoef(y, model_pred)
    if len(model.classes_)!=2:
        raise ValueError('A binary class problem is required')
    if model.classes_[1] == 1:
        pos_p = p[:,1]
    elif model.classes_[0] == 1:
        pos_p = p[:,0]
    
    #FIGURE
    plt.figure(figsize=[15,5])
    
    #1 -- Confusion matrix
    cm = confusion_matrix(y,model_pred)
    plt.subplot(131)
    ax = sb.heatmap(cm, annot=True, cmap='Blues', cbar=False, 
                annot_kws={"size": 14}, fmt='g', linewidth=.4, linecolor="k")
    ax.set(xticklabels=["Benign", "Pathogenic"], yticklabels=["Benign", "Pathogenic"])
    ax.tick_params(labelsize=10)
    cmlabels = ['True Negatives', 'False Positives',
              'False Negatives', 'True Positives']
    for i,t in enumerate(ax.texts):
        t.set_text(t.get_text() + "\n" + cmlabels[i])
    plt.title('Confusion Matrix', size=15)
    plt.xlabel('Predicted Values', size=13)
    plt.ylabel('True Values', size=13)
      
    #2 -- Distributions of Predicted Probabilities of both classes
    df = pd.DataFrame({'probPos':pos_p, 'target': y})
    plt.subplot(132)
    plt.hist(df[df.target==1].probPos, density=True, bins=25,
             alpha=.5, color='green',  label=labels[0])
    plt.hist(df[df.target==0].probPos, density=True, bins=25,
             alpha=.5, color='red', label=labels[1])
    plt.axvline(.5, color='blue', linestyle='--', label='Boundary', linewidth=1)
    plt.xlim([0,1])
    plt.title('Distributions of Predictions', size=15)
    plt.xlabel('Positive Probability (predicted)', size=13)
    plt.ylabel('Samples (normalized)', size=13)
    plt.legend(loc="upper right", prop={'size': 10})
    
    #3 -- ROC curve with annotated decision point
    fp_rates, tp_rates, _ = roc_curve(y,p[:,1])
    roc_auc = auc(fp_rates, tp_rates)
    plt.subplot(133)
    plt.plot(fp_rates, tp_rates, color='green',
             lw=1, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], lw=1, linestyle='--', color='grey')
    #plot current decision point:
    tn, fp, fn, tp = [i for i in cm.ravel()]
    plt.plot(fp/(fp+tn), tp/(tp+fn), 'bo', markersize=8, label='Decision Point')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate', size=13)
    plt.ylabel('True Positive Rate', size=13)
    plt.title('ROC Curve', size=15)
    plt.legend(loc="lower right", prop={'size': 10})
    plt.subplots_adjust(wspace=.3)
    #Print and Return the F1 score
    tn, fp, fn, tp = [i for i in cm.ravel()]
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    F1 = 2*(precision * recall) / (precision + recall)
    printout = '%s \n Precision: %0.4f |\
    Recall: %0.4f |\
    MCC: %0.4f |\
    MAE: %0.4f' % (model_name, precision, recall, model_mcc, model_mae)
    plt.suptitle(printout, y=-.05, fontsize=12)
    return F1

# Takes in a model, trains the model, and evaluates the model on the test set
def fit_and_evaluate(model):
    
    # Train the model
    model.fit(X, y)
    # Make predictions and evalute
    model_pred = model.predict(X_test)
    model_mae = mae(y_test, model_pred)
    # Return the performance metric
    return model_mae


# -

lr = LogisticRegression()
lr_mae = fit_and_evaluate(lr);
print('Logistic Regression Performance on the test set: MAE = %0.4f' % lr_mae)

# +
svc = SVC(C = 1000, gamma = 0.1)
svm_mae = fit_and_evaluate(svc)

print('Support Vector Machine Regression Performance on the test set: MAE = %0.4f' % svm_mae)

# +
random_forest = RandomForestClassifier(random_state=60)
random_forest_mae = fit_and_evaluate(random_forest)

print('Random Forest Classifier Performance on the test set: MAE = %0.4f' % random_forest_mae)

# +
gradient_boosted = GradientBoostingClassifier(random_state=60)
gradient_boosted_mae = fit_and_evaluate(gradient_boosted)

print('Gradient Boosted Classifier Performance on the test set: MAE = %0.4f' % gradient_boosted_mae)

# +
knn = KNeighborsClassifier(n_neighbors=10)
knn_mae = fit_and_evaluate(knn)

print('K-Nearest Neighbors Classifier Performance on the test set: MAE = %0.4f' % knn_mae)

# +
mnb = MultinomialNB()
mnb_mae = fit_and_evaluate(mnb)

print('Naive Bayes Performance on the test set: MAE = %0.4f' % mnb_mae)

# +

figsize(8, 6)

# Baseline guess is based on distribution of score in training set
baseline_df = train_labels['score'].value_counts().apply(lambda x: x / len(train_labels))
classes = baseline_df.index.values
probs = baseline_df.values
baseline_guess = np.random.choice(classes, size=len(y_test), p=probs)
baseline_guess_mae = mae(y_test, baseline_guess)

# Dataframe to hold the results
model_comparison = pd.DataFrame({'model': ['Logistic Regression', 'Support Vector Machine',
                                           'Random Forest', 'Gradient Boosted',
                                            'K-Nearest Neighbors', 'Multinomial Naive Bayes', 
                                           'baseline guess'],
                                 'mae': [lr_mae, svm_mae, random_forest_mae, 
                                         gradient_boosted_mae, knn_mae, 
                                         mnb_mae, baseline_guess_mae]})

# Horizontal bar chart of test mae
model_comparison.sort_values('mae', ascending = False).plot(x = 'model', y = 'mae', 
                                                            kind = 'barh',
                                                            color="b")

# Plot formatting
plt.ylabel(''); plt.yticks(size = 14); plt.xlabel('Mean Absolute Error'); plt.xticks(size = 14)
plt.title('Model Comparison on Test MAE', size = 20);
# -

model_comparison

random_forest = RandomForestClassifier(random_state=60)
random_forest.fit(X, y)
fig = evalBinaryClassifier(random_forest, X_test, y_test)
plt.savefig("figures/UniFun_randomforest_default.pdf", format="pdf", bbox_inches='tight')

for p,v in random_forest.get_params().items():
    print("%s : %s" % (p, str(v)))

# +
# These are the teo most important hyperparams
# Number of trees in random forest
n_estimators = [int(x) for x in np.linspace(start = 200, stop = 2000, num = 10)]
# Number of features to consider at every split
max_features = ['auto', 'sqrt']


# Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(10, 110, num = 11)]
max_depth.append(None)
# Minimum number of samples required to split a node
min_samples_split = [2, 5, 10]
# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 4]
# Method of selecting samples for training each tree
bootstrap = [True, False]

# Define the grid of hyperparameters to search
hyperparameter_grid = {'n_estimators': n_estimators,
                       'max_depth': max_depth,
                       'min_samples_split': min_samples_split,
                       'max_features': max_features,
                       'min_samples_leaf' :  min_samples_leaf,
                      'bootstrap' : bootstrap}

# +
# Create the model to use for hyperparameter tuning
model = RandomForestClassifier(random_state=60)

# Set up the random search with 4-fold cross validation
random_cv = RandomizedSearchCV(estimator=model,
                               param_distributions=hyperparameter_grid,
                               cv=4, n_iter=25, 
                               scoring = 'neg_mean_absolute_error',
                               n_jobs = -1, verbose = 1, 
                               return_train_score = True,
                               random_state=60)


# +
# Fit on the training data
random_cv.fit(X, y)
# Get all of the cv results and sort by the test performance
random_results = pd.DataFrame(random_cv.cv_results_).sort_values('mean_test_score', ascending = False)

random_results.head(10)
# -

# The best random forest classifier has the following params
d = random_cv.best_estimator_.get_params()
for p,v in d.items():
    print("%s : %s" % (p, str(v)))

# +
# Create a range of trees to evaluate
trees_grid = {'n_estimators': np.arange(10, 100, 5)}

model = RandomForestClassifier(max_leaf_nodes = None,
    random_state = 60,
    verbose = 0,
    min_samples_leaf = 1,
    warm_start = False,
    max_features = "auto",
    criterion = "gini",
    class_weight = None,
    bootstrap = False,
    min_samples_split = 5,
    min_weight_fraction_leaf = 0.0,
    n_jobs = 1,
    min_impurity_split = None,
    max_depth = 30,
    oob_score = False,
    min_impurity_decrease = 0.0)

# Grid Search Object using the trees range and the random forest model
grid_search = GridSearchCV(estimator = model, param_grid=trees_grid, cv = 4, 
                           scoring = 'neg_mean_absolute_error', verbose = 1,
                           n_jobs = -1, return_train_score = True)
# -

# Fit the grid search
grid_search.fit(X, y)

# +
# Get the results into a dataframe
results = pd.DataFrame(grid_search.cv_results_)

# Plot the training and testing error vs number of trees
figsize(8, 8)

plt.plot(results['param_n_estimators'], -1 * results['mean_test_score'], label = 'Testing Error')
plt.plot(results['param_n_estimators'], -1 * results['mean_train_score'], label = 'Training Error')
plt.xlabel('Number of Trees'); plt.ylabel('Mean Abosolute Error'); plt.legend();
plt.title('Performance vs Number of Trees');
# -

results.sort_values('mean_test_score', ascending = False).head(5)

# +
# Default model
default_model = RandomForestClassifier(random_state = 60)

# Select the best model
final_model = grid_search.best_estimator_

final_model
# -

# %%timeit -n 1 -r 5
default_model.fit(X, y)

# %%timeit -n 1 -r 5
final_model.fit(X, y)

# +
default_pred = default_model.predict(X_test)
final_pred = final_model.predict(X_test)
# save final model
filename = 'data/UniFun_final_model.sav'
pickle.dump(final_model, open(filename, 'wb'))

print('Default model performance on the test set: MAE = %0.4f.' % mae(y_test, default_pred))
print('Final model performance on the test set:   MAE = %0.4f.' % mae(y_test, final_pred))
# -

fig = evalBinaryClassifier(final_model, X_test, y_test)
plt.savefig("figures/UniFun_randomforest_final.pdf", format="pdf", bbox_inches='tight')
