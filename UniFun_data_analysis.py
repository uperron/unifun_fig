# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: py3.6
#     language: python
#     name: python3
# ---

# # A structurally-aware variant effect model
#
#
#
#
#
# ## Introduction
#
# Genetic variant effect prediction algorithms are used extensively in clinical genomics and research to determine the likely consequences of amino acid substitutions on protein function and their pathogenic significance. Popular variant effect prediction methods include [SIFT](https://dx.doi.org/10.1093/nar/gks539), [PolyPhen (v2)](https://dx.doi.org/10.1038/nmeth0410-248), [CADD](https://dx.doi.org/10.1038/ng.2892), [fathmm](https://dx.doi.org/10.1002%2Fhumu.22225) and [REVEL](https://reference.medscape.com/viewpublication/2412). These use information about local sequence phylogenetic conservation, amino acid physicochemical properties, functional domains and amino acid structural attributes in the case of PolyPhen. CADD and REVEL instead integrate and weight predictions from collections of tools.
# Variant predictors are widely used in practice, however their performance is strongly influenced by the choice and composition of variant datasets used for training and validation. REVEL for example achieves a ROC AUC of .96 when tested against a set of variants from Clinvar and of .63 when benchmarked against an assay-validated variant [dataset](https://doi.org/10.1186/s40246-017-0104-8).
# Thus, the devopment of more robust variant effect models is a viable area of research with a large translational potential. In this context residue-specific structural features could play an important role alogside sequence conservation and domain features; particularly when cosidering the relationship between protein structure and function and the increase in quality and quantity of solved strucutures. The strucutural features used here differ from dose in tools such as PolyPhen since they are computed for each residue in its unique local context as opposed to being general amino acid structural features computed across many different structures.
#
#
# ## Workflow
#
# In order to compute structural features it is necessary to map a variant's coordinates in the genome to the corresponding residue in our model of the protein coded by that genome region. Tools such as [VarMap](https://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/VarSite/GetPage.pl?uniprot_acc=P01308&template=VarMap.html) have automated this procedure. Here, however, genome-to-structure mapping was done from scratch utilizing the [ensembldb](https://doi.org/doi:10.18129/B9.bioc.ensembldb) and [biomaRt](https://doi.org/doi:10.18129/B9.bioc.biomaRt) R packages, the Uniprot API and the PDBe API.
#
# Three variant datasets are used: 
# - [UniFun](https://melbourne.figshare.com/articles/UniFun/4704955) is derived using protein annotation data from UniProt, specifically results from human protein mutagenesis experiments assessed for their effects on protein function
# - [BRCA1-DMS](https://figshare.com/articles/BRCA1-DMS/4704937) consists of deep mutational scanning variants on BRCA1
# - [TP53-TA](https://figshare.com/articles/TP53-TA/4704961) is an assessment of TP53 variants by transactivation assay.
#
# Structural features are then computed for each residue using [DSSP](http://swift.cmbi.ru.nl/gv/dssp/), [get_cleft](https://github.com/NRGlab/Get_Cleft) and [biopython](https://github.com/biopython/biopython).
#
# Code for all the steps above is available on [bitbucket](https://bitbucket.org/uperron/unifun).

# +
# this borrows from https://github.com/WillKoehrsen/machine-learning-project-walkthrough

import pandas as pd
import numpy as np
import pickle

# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)

import matplotlib.pyplot as plt
# %matplotlib inline


# Set default font size
plt.rcParams['font.size'] = 24

# Internal ipython tool for setting figure size
from IPython.core.pylabtools import figsize

import seaborn as sb
# Set default font size
sb.set(font_scale = 1.1)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)

# Splitting data into training and testing
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
# -

data = pd.read_csv("data/UniFun_BOTH.ALLfeatures.merged", sep="\t", header=0, index_col=None)
data.drop_duplicates(inplace=True)
data.head()

data.info()

# +
data["cleft_size"].fillna(0, inplace=True)
data["in_cleft"] = data["cleft_size"].apply(lambda x: 1 if x != 0 else 0)

M = data["cleft_size_rank"].max()
data["cleft_size_rank"].fillna(M, inplace=True)
data = data.replace({'NA': np.nan})
for col in [c for c in data.columns if c not in ["ID", "PRED", "SS_DSSP"]]:
    data[col] = data[col].astype(float)
# -

data.describe()

data.isnull().sum()


# Function to calculate missing values by column
# from https://stackoverflow.com/a/39734251
def missing_values_table(df):
        # Total missing values
        mis_val = df.isnull().sum()
        
        # Percentage of missing values
        mis_val_percent = 100 * df.isnull().sum() / len(df)
        
        # Make a table with the results
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
        
        # Rename the columns
        mis_val_table_ren_columns = mis_val_table.rename(
        columns = {0 : 'Missing Values', 1 : '% of Total Values'})
        
        # Sort the table by percentage of missing descending
        mis_val_table_ren_columns = mis_val_table_ren_columns[
            mis_val_table_ren_columns.iloc[:,1] != 0].sort_values(
        '% of Total Values', ascending=False).round(1)
        
        # Print some summary information
        print ("Your selected dataframe has " + str(df.shape[1]) + " columns.\n"      
            "There are " + str(mis_val_table_ren_columns.shape[0]) +
              " columns that have missing values.")
        
        # Return the dataframe with missing information
        return mis_val_table_ren_columns


#rule of thumb: reject columns with >50% missing values
missing_values_table(data)

# Most features have relatively few missing data, however "EXP_DSSP_RASA" might be worth further attenction as is both the worst offender and, retrospectively, the most predictive features.

data["SS_DSSP"].value_counts()

# encode prediction as binary: {"deleterious" : 1, "benign" : 0}
data["PRED"] = data["PRED"].map({"deleterious" : 1, "benign" : 0})
data = data.rename(columns = {'PRED': 'score'})

# +
figsize(8, 8)
# Histogram of the Energy Star Score

sb.countplot(x='score', data=data);
plt.xlabel('Score'); plt.ylabel('Number of variants'); 
plt.title("Raw counts by score", size = 22);
# -

# Overall the variant dataset has a class imbalance problem. This appears to be common across several datasets as "Pathogenic" variants appears to be more common (or more commonly reported) than "Benign" variants. Although this imbalance is not extreme (~75% of varinats are Pathogenic) it might be necessary to consider adjusting for this using sampling-based methods.

# +
# https://blog.dominodatalab.com/imbalanced-datasets/
# https://github.com/scikit-learn-contrib/imbalanced-learn

# +
# Score distibution by secondary structure
from itertools import product

figsize(12, 10)

types = data["SS_DSSP"].value_counts()
# only sencondary structure classes with more than 100 variants
types = list(types[types.values > 100].index)
subset = data.query("SS_DSSP in @types")[['score', 'SS_DSSP']].dropna()

# normalize by number of variants in score class
arr = []
for t,s in product(types, [1, 0]):
    n = len(subset.query("score ==@s & SS_DSSP == @t"))
    N = len(subset[subset['score'] == s])
    arr.append([t,s, n/N])
plot_df = pd.DataFrame(arr, columns=["SS_DSSP", "score", "norm_counts"])

sb.barplot(x = 'SS_DSSP', y="norm_counts", hue="score", data=plot_df);
plt.xlabel('Score', size = 20); plt.ylabel('Counts', size = 20); 
plt.title("Normalized counts by Score and II structure type", size = 22);
# -

# Secondary structure type does not seem to be strongly correlated with variant phenotype. However, some types show a larger difference in frequency (B-sheets, turns) and others almost none (unassigned residues).

# +
# Score distribution by solvent exposure as in EXP_DSSP_RASA
figsize(12, 10)
subset = data[['score', 'EXP_DSSP_RASA']]

for s in [0,1]:
    plot_data = subset[subset['score'] == s]
    # Density plot of exposure
    sb.kdeplot(plot_data['EXP_DSSP_RASA'].dropna(), label = str(s))
    
plt.xlabel('Score', size = 20); plt.ylabel('Density', size = 20); 
plt.title('Density Plot of score by EXP_DSSP_RASA', size = 22);

# +
# Score distribution by solvent exposure as in ExposureCN
figsize(12, 10)
subset = data[['score', 'ExposureCN']]

for s in [0,1]:
    plot_data = subset[subset['score'] == s]
    # Density plot of exposure
    sb.kdeplot(plot_data['ExposureCN'].dropna(), label = str(s))
    
plt.xlabel('Score', size = 20); plt.ylabel('Density', size = 20); 
plt.title('Density Plot of score by ExposureCN', size = 22);
# -

# Both figures show how solvent accessibility, regardless of the specific measure used, does have an effect on phenotype.
# This is consistent with what described in the [PoliPhen methods](http://genetics.bwh.harvard.edu/pph2/dokuwiki/_media/nmeth0410-248-s1.pdf), where features measuring "accessible surface area" and "change in accessible surface area propensity" were included among the most predictive.

# +
# Score distribution by cleft_size_rank
figsize(12, 10)
subset = data[['score', 'cleft_size_rank']]

for s in [0,1]:
    plot_data = subset[subset['score'] == s]
    # Density plot of exposure
    sb.kdeplot(plot_data['cleft_size_rank'].dropna(), label = str(s))
    
plt.xlabel('Score', size = 20); plt.ylabel('Density', size = 20); 
plt.title('Density Plot of score by cleft_size_rank', size = 22);
# -

# Cleft size does seem to have a small effectas well on variant phenotype.

# Find all correlations and sort 
correlations_data = data.corr()['score'].sort_values()
print(correlations_data, '\n')


# Both the amino acid (RUM20) and the codon exchangeabilities seem to have almost no correlation to phenotype.

# +
# Select the numeric columns
numeric_subset = data.select_dtypes('number')

# Create columns with square root and log of numeric columns
for col in numeric_subset.columns:
    # Skip the Energy Star Score column
    if col == 'score':
        next
    else:
        numeric_subset['sqrt_' + col] = np.sqrt(numeric_subset[col])
        numeric_subset['log_' + col] = np.log(numeric_subset[col])

# Select the categorical columns
categorical_subset = data["SS_DSSP"]

# One hot encode
categorical_subset = pd.get_dummies(categorical_subset)

# Join the two dataframes using concat
# Make sure to use axis = 1 to perform a column bind
features = pd.concat([numeric_subset, categorical_subset], axis = 1)

# Find correlations with the score 
correlations = features.corr()['score'].dropna().sort_values()
print(correlations, '\n')

# +
# Score distribution by sqrt_ExposureCN
figsize(12, 10)
subset = features[['score', 'sqrt_ExposureCN']]

for s in [0,1]:
    plot_data = subset[subset['score'] == s]
    # Density plot of exposure
    sb.kdeplot(plot_data['sqrt_ExposureCN'].dropna(), label = str(s))
    
plt.xlabel('Score', size = 20); plt.ylabel('Density', size = 20); 
plt.title('Density Plot of score by sqrt_ExposureCN', size = 22);

# +
# Score distribution by sqrt_ExposureCN
figsize(12, 10)
subset = features[['score', 'sqrt_PHI_DSSP']]

for s in [0,1]:
    plot_data = subset[subset['score'] == s]
    # Density plot of exposure
    sb.kdeplot(plot_data['sqrt_PHI_DSSP'].dropna(), label = str(s))
    
plt.xlabel('Score', size = 20); plt.ylabel('Density', size = 20); 
plt.title('Density Plot of score by sqrt_PHI_DSSP', size = 22);

# +
# Extract the columns to  plot
plot_data = features[['score', 'sqrt_ExposureCN', 
                      'sqrt_PHI_DSSP', 'EXP_DSSP_RASA']]

# Replace the inf with nan
plot_data = plot_data.replace({np.inf: np.nan, -np.inf: np.nan})

# Drop na values
plot_data = plot_data.dropna()

# Function to calculate correlation coefficient between two columns
def corr_func(x, y, **kwargs):
    r = np.corrcoef(x, y)[0][1]
    ax = plt.gca()
    ax.annotate("r = {:.2f}".format(r),
                xy=(.2, .8), xycoords=ax.transAxes,
                size = 20)

# Create the pairgrid object
grid = sb.PairGrid(data = plot_data, size = 3)

# Upper is a scatter plot
grid.map_upper(plt.scatter, alpha = 0.6, color="g")

# Diagonal is a histogram
grid.map_diag(plt.hist, edgecolor = 'black')

# Bottom is correlation and density plot
grid.map_lower(corr_func);
grid.map_lower(sb.kdeplot, cmap = plt.cm.Reds)

# Title for entire plot
plt.suptitle('Pairs Plot of structural features', size = 20, y = 1.02);
# -

features.shape


def remove_collinear_features(x, threshold):
    #        Remove collinear features in a dataframe with a correlation coefficient
    #        greater than the threshold. Removing collinear features can help a model
    #        to generalize and improves the interpretability of the model.

    # Dont want to remove correlations between Energy Star Score
    y = x['score']
    x = x.drop(columns = ['score'])
    
    # Calculate the correlation matrix
    corr_matrix = x.corr()
    iters = range(len(corr_matrix.columns) - 1)
    drop_cols = []

    # Iterate through the correlation matrix and compare correlations
    for i in iters:
        for j in range(i):
            item = corr_matrix.iloc[j:(j+1), (i+1):(i+2)]
            col = item.columns
            row = item.index
            val = abs(item.values)
            
            # If correlation exceeds the threshold
            if val >= threshold:
                # Print the correlated features and the correlation value
                # print(col.values[0], "|", row.values[0], "|", round(val[0][0], 2))
                drop_cols.append(col.values[0])

    # Drop one of each pair of correlated columns
    drops = set(drop_cols)
    x = x.drop(columns = drops)
    
    # Add the score back in to the data
    x['score'] = y
               
    return x


# Remove the collinear features above a specified correlation coefficient
features = remove_collinear_features(features, 0.6);

# Remove any columns with all na values
features  = features.dropna(axis=1, how = 'all')
features.shape

# +
score = features[features['score'].notnull()]
# Separate out the features and targets
features = score.drop(columns='score')
targets = pd.DataFrame(score['score'])

# Replace the inf and -inf with nan (required for later imputation)
features = features.replace({np.inf: np.nan, -np.inf: np.nan})

# Split into 70% training and 30% testing set
X, X_test, y, y_test = train_test_split(features, targets, test_size = 0.3, random_state = 42)

print(X.shape)
print(X_test.shape)
print(y.shape)
print(y_test.shape)
# -

print(features.columns)

# +
from sklearn.metrics import confusion_matrix

# https://scikit-learn.org/stable/auto_examples/model_selection/\
# plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
# Plot the confusion matrix to evaluate the quality of the output of a classifier.
# The diagonal elements represent the number of points for which the 
# predicted label is equal to the true label, while off-diagonal elements 
# are those that are mislabeled by the classifier. The higher the diagonal 
# values of the confusion matrix the better, indicating many correct predictions.
def plot_confusion_matrix(y_true, y_pred, normalize=False, cmap=plt.cm.Blues):

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Compute accuracy
    accuracy = np.sum(np.diagonal(cm)) / np.sum(cm)
    # Only use the labels that appear in the data
    classes = sorted(unique_labels(y_true, y_pred))
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        title = "Normalized confusion matrix, accuracy = %f " % (accuracy)
    else:
        title = "Confusion matrix, accuracy = %f " % (accuracy)


    fig, ax = plt.subplots(figsize=(8, 7))
    im = sb.heatmap(cm,  annot=True, fmt="d", 
                    linewidth=.5,
                    linecolor="k",
                    cmap=cmap)
    ax.set(xticklabels=["Benign", "Pathogenic"], yticklabels=["Benign", "Pathogenic"])
    # We want to show all ticks...
    ax.set(ylabel='True label',
          xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    plt.suptitle(title, size = 20, y = 1.02);
    fig.tight_layout()
    return ax

# Function to calculate mean absolute error
def mae(y_true, y_pred):
    return np.mean(abs(y_true - y_pred))



# -

# Baseline guess is based on distribution of score in training set
baseline_df = y['score'].value_counts().apply(lambda x: x / len(y))
classes = baseline_df.index.values
probs = baseline_df.values
baseline_guess = np.random.choice(classes, size=len(y_test), p=probs)
plot_confusion_matrix(y_test, baseline_guess);
print("Baseline mean abbsolute error: %.4f" % mae(y_test.values, baseline_guess));


X.to_csv('data/UniFun_training_features.csv', index = False)
X_test.to_csv('data/UniFun_testing_features.csv', index = False)
y.to_csv('data/UniFun_training_labels.csv', index = False)
y_test.to_csv('data/UniFun_testing_labels.csv', index = False)
